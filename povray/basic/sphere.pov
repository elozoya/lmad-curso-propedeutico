#include "colors.inc"

background { color Cyan }

camera {
  location <0, 0, -10>
  look_at <0, 0, 0>
}

sphere {
  <0, 0, 0>, 2
  texture {
    pigment { color Yellow }
  }
}
sphere {
  <5, 0, 0>, 1
  texture {
    pigment { color rgb <1.0, 0.0, 0.0> }
  }
}

light_source { <2, 4, -3> color White}

