#include "colors.inc"    // The include files contain
#include "stones.inc"    // pre-defined scene elements

camera {
  location <0, 2, -3>
  look_at  <0, 1,  2>
}

cylinder {
  <0, 1, 0>,     // Center of one end
  <1, 2, 3>,     // Center of other end
  0.5            // Radius
  open           // Remove end caps
  texture { T_Stone25 scale 4 }
}

light_source { <2, 4, -3> color White}
