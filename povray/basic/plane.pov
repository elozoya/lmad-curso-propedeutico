#include "colors.inc"    // The include files contain
#include "stones.inc"    // pre-defined scene elements

camera {
  location <0, 2, -3>
  look_at  <0, 1,  2>
}

plane { <0, 1, 0>, -1
  pigment {
    checker color Red, color Blue
  }
}

light_source { <2, 4, -3> color White}
