#include "colors.inc"
background { color Cyan }

camera {
  location <0, 0, -10>
  look_at <0, 0, 0>
}
light_source { <2, 4, -3> color White}

#declare RedBall = sphere {
  <0,0,0>,
  0.5
  texture {
    pigment{ color Red }
    finish { phong 1 }
  }
}
#declare BlueBall = sphere {
  <0,0,0>,
  0.5
  texture {
    pigment{ color Blue }
    finish { phong 1 }
  }
}

#for (Cntr, 0, 7, 1)
   object{
  #if (mod(Cntr, 3) = 0)
      RedBall
  #else
      BlueBall
  #end
     translate <2, 0, 0>
     rotate <0, 0, Cntr * 45>
   }
#end

