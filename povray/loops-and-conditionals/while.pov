#include "colors.inc"
background { color Cyan }

camera {
  location <0, 0, -10>
  look_at <0, 0, 0>
}
light_source { <2, 4, -3> color White}

#declare Ball = sphere {
  <0,0,0>, 0.5
  texture {
    pigment{ color Red }
    finish { phong 1 }
  }
}

#declare X = -5;
#declare End_X = 5;
#while ( X <= End_X )
 object{ Ball translate < X, 0.5, 0> }
#declare X = X + 1;
#end

