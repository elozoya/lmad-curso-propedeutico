#include "colors.inc"
background { color Cyan }
light_source { <2, 4, -3> color White}

camera {
  location <0, 0, -10>
  look_at <0, 0, 0>
}

sphere {
  <0, 0, 0>, 2
  texture {
    pigment { color Yellow }
  }
  translate <3, 0, 0>
  rotate <0, 360 * clock, 0>
}
text {
  ttf "timrom.ttf" str(clock, 0, 4) 0.2, 0
  pigment { Red }
  translate <3, 3, 0>
}

