camera { location <0,0,-10> look_at <0,0,0> angle 35 }
light_source { <10,20,-40> color 1 }

intersection
{ sphere { <-1,0,0>,2 }
  sphere { <1,0,0>,2 }
  pigment { color <1,1,1> transmit 0.5 }
}

