camera { location <0,0,-10> look_at <0,0,0> angle 35 }
light_source { <10,20,-40> color 1 }


difference
{ sphere { <-1,0,0>,2 pigment { color <1,1,0> } }
  sphere { <1,0,0>,2 pigment { color <0,1,0> } }
  rotate <0,30,0>
}

