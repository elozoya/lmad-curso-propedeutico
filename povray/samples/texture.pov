#include "colors.inc"
#include "textures.inc"

background { color Cyan }
light_source { <2, 4, -3> color White}

camera {
  location <3, 5, -10>
  look_at <0, 0, 0>
}

plane {
  <0, 1, 0>, -2
  pigment {checker White Tan}
}

sphere {
  <0, 0, 0>, 1
  texture {
    pigment { Jade }
    finish { phong 1.0 }
  }
}
sphere {
  <2, 0, 0>, 1
  texture {
    Cork
    finish { phong 1.0 }
  }
}
sphere {
  <4, 0, 0>, 1
  material { M_Glass3 }
}
sphere {
  <-2, 0, 0>, 1
  texture {
    pigment { Jade }
    finish { phong 1.0 }
    normal {bumps 1 scale 0.02}
  }
}
// Example for "filter" and "transmit":
//--------------------------------------------- rgbf
sphere{ <0,0,-2>, 1
        texture { pigment{ color rgbf<1,0.7,0, 0.7>}
                  finish { diffuse 0.9 phong 0.5}
                } // end of texture
      } // end of sphere ---------------------------
//--------------------------------------------- rgbt
sphere{ <2,0,-2>, 1
        texture { pigment{ color rgbt<1,0.7,0,0.7>}
                  finish { diffuse 0.9 phong 0.5}
                } // end of texture
      } // end of sphere -------------------------
sphere{ <4, 0, -2>, 1
        texture{ Polished_Chrome } 
      } 
