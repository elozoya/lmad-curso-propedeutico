#include "colors.inc"
background { color Cyan }
light_source { <2, 4, -3> color White}

camera {
  location <3, 5, -10>
  look_at <0, 0, 0>
}

plane {
  <0, 1, 0>, -2
  pigment {checker White Tan}
}
sphere {
  <0, 0, 0>, 1
  pigment { color Red }
}

cylinder {
  <0, 0, 0>, <3, 0, 0>, 0.1
  pigment { Red }
}
cone {
  <0, 0, 0>, 0.5, <1, 0, 0> 0
  translate <3, 0, 0>
  pigment { Red }
}
text {
  ttf "timrom.ttf" "x" 0.2, 0
  pigment { Red }
  translate <4, 0, 0>
}

cylinder {
  <0, 0, 0>, <0, 3, 0>, 0.1
  pigment { Green }
}
cone {
  <0, 0, 0>, 0.5, <0, 1, 0> 0
  translate <0, 3, 0>
  pigment { Green }
}
text {
  ttf "timrom.ttf" "y" 0.2, 0
  pigment { Green }
  translate <0, 4, 0>
}

cylinder {
  <0, 0, 0>, <0, 0, 3>, 0.1
  pigment { Blue }
}
cone {
  <0, 0, 0>, 0.5, <0, 0, 1> 0
  translate <0, 0, 3>
  pigment { Blue }
}
text {
  ttf "timrom.ttf" "z" 0.2, 0
  pigment { Blue }
  translate <0, 0, 4>
}

