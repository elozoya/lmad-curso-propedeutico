// CSG - Constructive Solid Geometry
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"

background { color Cyan }
light_source { <2, 4, -3> color White}

camera {
  location <3, 5, -10>
  look_at <0, 0, 0>
}

plane {
  <0, 1, 0>, -2
  pigment {checker White Tan}
}

union{
  sphere{<0,1,0>,0.35}
  cone{<0,0,0>,0.45,<0,1.2,0>,0}
  texture{T_Glass3} interior{I_Glass}
  translate <-0.5, 0, 0>
     }
merge{ 
  sphere{<0,1,0>,0.35}
  cone{<0,0,0>,0.45,<0,1.2,0>,0}
  texture{T_Glass3} interior{I_Glass}
  translate < 0.5, 0, 0>
     }
