#include <iostream>
#include <cstdlib>
using namespace std;

//Print exit code
//windows - echo %ERRORLEVEL%
//unix - echo $?

int main(int argc, char** argv) {
  if (argc < 3) {
    cout << "Error: debes de proporcionar dos numeros" << endl;
    return 1;
  }
  int x = atoi(argv[1]);
  int y = atoi(argv[2]);
  int result = x + y;
  cout << result << endl;
  return 0;
}

