#include <iostream>
#include <limits>
using namespace std;

int main(int argc, char** argv) {
  int imin = std::numeric_limits<int>::min();
  int imax = std::numeric_limits<int>::max();
  cout << "Sizeof(int) = " << sizeof(int) << " bytes" << endl;
  cout << "imax = " << imax << endl;
  cout << "imin = " << imin << endl;
  cout << "imax + 1 = " << imax + 1 << endl;
  cout << "imin - 1 = " << imin - 1 << endl;
  return 0;
}

