# LMAD Curso Propedeutico

# Povray
- Getting Started - http://www.povray.org/documentation/3.7.0/t2_2.html
- Home - http://www.povray.org/documentation/3.7.0/index.html
- What is - http://www.povray.org/documentation/3.7.0/u1_0.html
- Reference - http://www.povray.org/documentation/3.7.0/r3_0.html
- Introduction - http://www.povray.org/documentation/3.7.0/t2_1.html#t2_1

# Povray models
- POV-Ray objects and scripts - http://www.oyonale.com/modeles.php?format=POV

# Povray animations
- 3D Animation with POV-Ray - http://www.f-lohmueller.de/pov_tut/animate/pov_anie.htm

# Povray CSG - Constructive Solid Geometry
- Povray beginner tutorial: CSG - http://www.cs.tut.fi/~tgraf/harjoitustyot/tutorial/tutorial1.6.html


# Util
- Convert images to video (terminal) - avconv -i ./animation-sphere%02d.png output.mp4

